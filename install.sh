sudo apt-get install git openjdk-19-jdk maven mysql-server

git clone https://gitlab.com/kkowalcz3333/library_program.git

cd library_program
mvn package

sudo mysql -u root -p='' <<'EOT'
CREATE USER 'librarian'@'localhost' IDENTIFIED BY 'libr_pswd';
GRANT CREATE ON `library\_program\_%`.* TO 'librarian'@'localhost';
GRANT ALL PRIVILEGES ON `library\_program\_%`.* TO 'librarian'@'localhost';
FLUSH PRIVILEGES;
quit
EOT

echo "cd $(echo "$PWD")" > run.sh
echo "java -jar target/PAP2023Z-Z22-1.0-SNAPSHOT-jar-with-dependencies.jar" >> run.sh
chmod 777 run.sh
./run.sh
