# Skrypt instalacyjny #

Dostępny jest skrypt instalacyjny (install.sh), który pobierze wszystkie zależności, sklonuje repozytorium, skompiluje program i go uruchomi. Instalując program w ten sposób, wystarczy pobrać plik, umieścić go w miejscu, gdzie ma powstać folder z programem, nadać mu prawa do wykonywania się: `chmod u+x install.sh` i go uruchomić: `sudo ./install.sh`. Skrypt tworzy też skrypt uruchamiający aplikację `run.sh`, który można skopiować w inne miejsce.

# Samodzielna instalacja #

Instalacja była testowana na Ubuntu w wersji 22.04.3. Projekt wymaga zainstalowania (`sudo apt-get install`) następujących paczek (instalacja powinna być niezależna od wersji i można zainstalować najnowsze paczki, ale gdyby pojawiły się problemy z kompatybilnością, podana kombinacja na pewno działa):
- git (`git=2.34.1`)
- środowisko Javy (`openjdk-19-jdk`)
- maven (`maven=3.6.3`)
- serwer MySQL (`mysql-server=8.0.35`)

W MySQL należy dodać użytkownika, poprzez którego program będzie łączył się z serwerem, i nadać mu uprawnienia do wybranych nazw baz danych (najlepiej nazw zaczynających się od jakiegoś prefixu, korzystając z wildcarda). Przy świeżej instalacji paczki hasło roota jest puste.<br>
`sudo mysql -u root -p=''`<br>
W konsoli MySQL: (wartości w backtickach są sformatowane inaczej ze względu na specyfikę README, linia wyświetla się poprawnie)<br>
`CREATE USER 'librarian'@'localhost' IDENTIFIED BY 'libr_pswd';`<br>
`GRANT CREATE ON ` \`library\\_program\\_%.*\` ` TO 'librarian'@'localhost';`<br>
`GRANT ALL PRIVILEGES ON ` \`library\\_program\\_%.*\` ` TO 'librarian'@'localhost';`<br>
`FLUSH PRIVILEGES;`<br>
`quit`<br>
Jeżeli w tych komendach zostaną użyte inne wartości, będzie trzeba je wstawić do konfiguracji programu (zakładka "Baza" w uruchomionej aplikacji).

W kolejnym etapie należy sklonować repozytorium w wybranej lokalizacji i zlecić Mavenowi utworzenie JARa:<br>
`git clone https://gitlab.com/kkowalcz3333/library_program.git`<br>
`cd library_program`<br>
`mvn package`<br>

Utworzoną paczkę można uruchomić komendą:<br>
`java -jar target/PAP2023Z-Z22-1.0-SNAPSHOT-jar-with-dependencies.jar`
