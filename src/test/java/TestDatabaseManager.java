import org.junit.Assert;
import org.junit.Test;
import z22.logic.*;

import java.sql.SQLException;
import java.util.Arrays;

public class TestDatabaseManager {
    @Test
    public void testBasic() throws SQLException {
        DatabaseManager.connect();
        DatabaseManager.deleteBase("testbase");
        DatabaseManager.createBase("testbase", true);
        Assert.assertTrue(Arrays.asList(DatabaseManager.getDatabaseNames()).contains("testbase"));
        DatabaseManager.setBase("testbase");
        Reader[] readers1 = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
        Assert.assertEquals("Ireneusz Stępień", readers1[0].getName());
        DatabaseManager.removeReader(readers1[0]);
        Reader[] readers2 = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
        Assert.assertNotEquals(readers1, readers2);
        DatabaseManager.reconnect();
        DatabaseManager.deleteBase("testbase");
        DatabaseManager.disconnect();
    }

    @Test
    public void testOperations() throws SQLException {
        DatabaseManager.connect();
        DatabaseManager.deleteBase("testbase");
        DatabaseManager.createBase("testbase", true);
        Borrowing[] borrowings = DatabaseManager.resultQuery("SELECT * FROM borrowings;", Borrowing::new, Borrowing[]::new);
        Author[] authors = DatabaseManager.resultQuery("SELECT * FROM authors;", Author::new, Author[]::new);
        Reader[] readers = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
        Title[] titles = DatabaseManager.resultQuery("SELECT * FROM titles;", Title::new, Title[]::new);
        Book[] books = DatabaseManager.resultQuery("SELECT * FROM books;", Book::new, Book[]::new);

        OperationManager.addOperationBorrowBook(titles[4], books[9], readers[0]); // book (10) of w pustyni (5) to ireneusz
        OperationManager.addOperationReturnBook(borrowings[1], titles[3], books[8], readers[1]); // book (9) of igrzyska smierci (4) from maria
        OperationManager.addOperationTakePayment(readers[4], 1500); // krystyna
        OperationManager.addOperationRemoveAuthor(authors[0]); // unknown author
        OperationManager.addOperationRemoveBook(titles[1], books[3]); // book (4) of quo vadis (2)
        OperationManager.addOperationRemoveTitle(titles[9]); // latarnik
        OperationManager.addOperationRemoveReader(readers[2]); // beata

        Assert.assertEquals(OperationManager.getOperations().size(), 7);
        OperationManager.removeOperation(1);
        OperationManager.removeAllOperations();

        OperationManager.addOperationBorrowBook(titles[4], books[9], readers[0]); // book (10) of w pustyni (5) to ireneusz
        OperationManager.addOperationReturnBook(borrowings[1], titles[3], books[8], readers[1]); // book (9) of igrzyska smierci (4) from maria
        OperationManager.addOperationTakePayment(readers[4], 1500); // krystyna
        OperationManager.addOperationRemoveAuthor(authors[0]); // unknown author
        OperationManager.addOperationRemoveBook(titles[1], books[3]); // book (4) of quo vadis (2)
        OperationManager.addOperationRemoveTitle(titles[9]); // latarnik
        OperationManager.addOperationRemoveReader(readers[2]); // beata

        OperationManager.executeOperation(2);
        OperationManager.executeOperation(2);
        OperationManager.executeOperation(2);
        OperationManager.executeOperation(2);
        OperationManager.executeOperation(2);

        borrowings = DatabaseManager.resultQuery("SELECT * FROM borrowings;", Borrowing::new, Borrowing[]::new);
        authors = DatabaseManager.resultQuery("SELECT * FROM authors;", Author::new, Author[]::new);
        readers = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
        titles = DatabaseManager.resultQuery("SELECT * FROM titles;", Title::new, Title[]::new);
        books = DatabaseManager.resultQuery("SELECT * FROM books;", Book::new, Book[]::new);

        Assert.assertEquals(borrowings.length, 3);
        Assert.assertEquals(authors.length, 4);
        Assert.assertEquals(readers.length, 4);
        Assert.assertEquals(titles.length, 7);
        Assert.assertEquals(books.length, 8);
        Assert.assertEquals(readers[3].getPayment(), 500);
        Assert.assertTrue(books[6].getAvailability());
        Assert.assertFalse(books[5].getAvailability());
        Assert.assertEquals(0, readers[1].getPayment());

        OperationManager.executeAllOperations();

        borrowings = DatabaseManager.resultQuery("SELECT * FROM borrowings;", Borrowing::new, Borrowing[]::new);
        authors = DatabaseManager.resultQuery("SELECT * FROM authors;", Author::new, Author[]::new);
        readers = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
        titles = DatabaseManager.resultQuery("SELECT * FROM titles;", Title::new, Title[]::new);
        books = DatabaseManager.resultQuery("SELECT * FROM books;", Book::new, Book[]::new);

//        Assert.assertFalse(books[6].getAvailability());
//        Assert.assertTrue(books[5].getAvailability());
//        Assert.assertNotEquals(0, readers[1].getPayment());
//        Assert.assertEquals(borrowings.length, 3);
//        Assert.assertEquals(borrowings[2].getIdBook(), 10);

        DatabaseManager.deleteBase("testbase");
        DatabaseManager.disconnect();
    }
}

