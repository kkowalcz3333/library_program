package z22.logic;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Function;

/** The "static" class dealing with database connection and queries. */
public class DatabaseManager {
    private static Connection connection;
    private static Config config;
    private static String configPath;

    static {
        configPath = System.getProperty("user.dir");
        if (configPath.endsWith("/target")) {
            // cut /target if the program is run from jar
            configPath = configPath.substring(0, configPath.length() - 7);
        }
        configPath += "/config.properties";

        DatabaseManager.loadConfig();
    }

    public static String[] getDatabaseNames() {
        LinkedList<String> list = new LinkedList<>();
        try {
            if (connection == null || connection.isClosed()) return new String[]{};

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SHOW DATABASES;");
            while(result.next()) {
                String name = result.getString(1);
                if (name.contains(config.getDatabaseNamePrefix())) {
                    list.add(name.substring(config.getDatabaseNamePrefix().length()));
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        String[] array = new String[list.size()];
        list.toArray(array);
        return array;
    }

    public static String getConfigPath() {
        return configPath;
    }

    public static void loadConfig() {
        try {
            config = new Config(configPath);
        } catch(Exception ex) {
            System.out.println("Path: " + configPath);
            ex.printStackTrace();
        }
    }

    public static void setBase(String name) {
        resultlessQuery("USE " + config.getDatabaseNamePrefix() + name + ";");
    }

    public static void connect() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306", config.getDatabaseUser(), config.getDatabasePassword());
    }

    public static void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            System.out.println("Unable to close connection " + ex.getMessage());
        }

    }

    public static void reconnect() throws SQLException {
        if (connection != null && !connection.isClosed()) disconnect();
        connect();
    }

    public static void resultlessQuery(String query) {
        try {
            connection.createStatement().execute(query);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static <T> T[] resultQuery(String query, Function<ResultSet, T> function, Function<Integer, T[]> arrayMaker) {
        ArrayList<T> list = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(query);
            while(result.next()) {
                list.add(function.apply(result));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        T[] array = arrayMaker.apply(list.size());
        return list.toArray(array);
    }

    public static int calculatePayment(Borrowing borrowing) {
        return borrowing.getDelay() * config.getPaymentPerDay();
    }

    public static String calculateFormattedPayment(Borrowing borrowing) {
        return Reader.getFormattedPayment(borrowing.getDelay() * config.getPaymentPerDay());
    }

    public static void borrowBook(Title title, Book book, Reader reader) {
        // TODO: add necessary queries
        System.out.println("Borrowing book " + book.getIdBook() + " titled " + title.getTitle() + " to reader " + reader.getName());
    }

    public static void returnBook(Borrowing borrowing, Title title, Book book, Reader reader) {
        // TODO: add necessary queries
        System.out.println("Taking borrowed book " + book.getIdBook() + " titled " + title.getTitle() + " from reader " + reader.getName() + " and billing them " + DatabaseManager.calculateFormattedPayment(borrowing));
    }

    public static void takePayment(Reader reader, int valueGr) {
        resultlessQuery("UPDATE readers SET payment = payment - " + valueGr + " WHERE id_reader = " + reader.getIdReader() + ";");
    }

    public static void removeBook(Book book) {
        resultlessQuery("DELETE FROM books WHERE id_book = " + book.getIdBook() + ";");
    }

    public static void removeTitle(Title title) {
        resultlessQuery("DELETE FROM titles WHERE id_title = " + title.getIdTitle() + ";");
    }

    public static void removeReader(Reader reader) {
        resultlessQuery("DELETE FROM readers WHERE id_reader = " + reader.getIdReader() + ";");
    }

    public static void removeAuthor(Author author) {
        resultlessQuery("DELETE FROM authors WHERE id_author = " + author.getIdAuthor() + ";");
    }

    public static void deleteBase(String name) {
        resultlessQuery("DROP DATABASE IF EXISTS `" + config.getDatabaseNamePrefix() + name + "`;");
    }

    public static void createBase(String name, boolean fillWithData) {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP DATABASE IF EXISTS `" + config.getDatabaseNamePrefix() + name + "`;");
            statement.execute("CREATE DATABASE `" + config.getDatabaseNamePrefix() + name + "`;");
            statement.execute("USE `" + config.getDatabaseNamePrefix() + name + "`;");
            statement.execute("CREATE TABLE authors (id_author INT NOT NULL AUTO_INCREMENT, author_name VARCHAR(100) NOT NULL, author_image BLOB, PRIMARY KEY (id_author));");
            statement.execute("CREATE TABLE readers (id_reader INT NOT NULL AUTO_INCREMENT, reader_name VARCHAR(100) NOT NULL, payment INT NOT NULL DEFAULT 0, phone_number BIGINT, email VARCHAR(100), PRIMARY KEY (id_reader));");
            statement.execute("CREATE TABLE titles (id_title INT NOT NULL AUTO_INCREMENT, id_author INT NOT NULL, title VARCHAR(250) NOT NULL, isbn VARCHAR(40), publisher VARCHAR(200), year_published INT, pages INT, title_image BLOB, PRIMARY KEY (id_title), FOREIGN KEY (id_author) REFERENCES authors(id_author) ON DELETE CASCADE);");
            statement.execute("CREATE TABLE books (id_book INT NOT NULL AUTO_INCREMENT, id_title INT NOT NULL, bookstand INT NOT NULL, shelf INT NOT NULL, availability INT NOT NULL DEFAULT 1, PRIMARY KEY (id_book), FOREIGN KEY (id_title) REFERENCES titles(id_title) ON DELETE CASCADE);");
            statement.execute("CREATE TABLE borrowings (id_borrowing INT NOT NULL AUTO_INCREMENT, id_book INT NOT NULL, id_reader INT NOT NULL, date_return DATE NOT NULL, PRIMARY KEY (id_borrowing), FOREIGN KEY (id_book) REFERENCES books(id_book) ON DELETE CASCADE, FOREIGN KEY (id_reader) REFERENCES readers(id_reader) ON DELETE CASCADE);");

            if (!fillWithData) return;
            PreparedStatement insertStatement;

            statement.execute("INSERT INTO authors (author_name) VALUES ('(autor nieznany)');");
            insertStatement = connection.prepareStatement("INSERT INTO authors (author_name, author_image) VALUES('Henryk Sienkiewicz', x?);");
            insertStatement.setString(1, FileProcessor.resourceToHex("/sienkiewicz.jpg"));
            insertStatement.execute();
            insertStatement = connection.prepareStatement("INSERT INTO authors (author_name, author_image) VALUES('Adam Mickiewicz', x?);");
            insertStatement.setString(1, FileProcessor.resourceToHex("/mickiewicz.jpg"));
            insertStatement.execute();
            insertStatement = connection.prepareStatement("INSERT INTO authors (author_name, author_image) VALUES('J. R. R. Tolkien', x?);");
            insertStatement.setString(1, FileProcessor.resourceToHex("/tolkien.jpg"));
            insertStatement.execute();
            statement.execute("INSERT INTO authors (author_name) VALUES ('Suzanne Collins');");

            statement.execute("INSERT INTO readers (reader_name, phone_number, email) VALUES ('Ireneusz Stępień', 48605309233, 'i.stepien@wp.pl');");
            statement.execute("INSERT INTO readers (reader_name, phone_number) VALUES ('Maria Kamińska', 48856913931);");
            statement.execute("INSERT INTO readers (reader_name, email) VALUES ('Beata Woźniak', 'b.wozniak@gmail.com');");
            statement.execute("INSERT INTO readers (reader_name) VALUES ('Eryk Kaźmierczak');");
            statement.execute("INSERT INTO readers (reader_name, payment) VALUES ('Krystyna Adamska', 2000);");

            insertStatement = connection.prepareStatement("INSERT INTO titles (id_author, title, isbn, publisher, year_published, pages, title_image) VALUES(3, 'Pan Tadeusz, czyli ostatni zajazd na Litwie: historia szlachecka z roku 1811 i 1812 we dwunastu księgach wierszem', '978-83-8279-629-2', 'Siedmioróg', 2022, 434, x?);");
            insertStatement.setString(1, FileProcessor.resourceToHex("/pan_tadeusz.jpeg"));
            insertStatement.execute();
            statement.execute("INSERT INTO titles (id_author, title, isbn, publisher, year_published, pages) VALUES (2, 'Quo vadis', '978-83-7327-187-6', 'Greg', 2021, 472);");
            statement.execute("INSERT INTO titles (id_author, title, isbn, publisher, pages) VALUES (1, 'Pieśń o Rolandzie', '978-83-779-1711-4', 'Znak', 114);");
            statement.execute("INSERT INTO titles (id_author, title, isbn, publisher, year_published, pages) VALUES (5, 'Igrzyska śmierci. Tom 3. Kosogłos', '978-83-7278-491-9', 'Media Rodzina', 2010, 372);");
            statement.execute("INSERT INTO titles (id_author, title, isbn, publisher, year_published, pages) VALUES (2, 'W pustyni i w puszczy', '978-83-7517-647-6', 'Greg', 2021, 400);");
            statement.execute("INSERT INTO titles (id_author, title, isbn, publisher, year_published, pages) VALUES (3, 'Dziady', '978-83-8279-664-3', 'Siedmioróg', 2022, 282);");
            statement.execute("INSERT INTO titles (id_author, title) VALUES (5, 'Igrzyska śmierci');");
            statement.execute("INSERT INTO titles (id_author, title, pages) VALUES (1, 'Epos o Gilgameszu', 105);");
            statement.execute("INSERT INTO titles (id_author, title, publisher, year_published, pages) VALUES (5, 'Igrzyska śmierci. Tom 2. W pierścieniu ognia', 'Media Rodzina', 2022, 360);");
            statement.execute("INSERT INTO titles (id_author, title, isbn) VALUES (2, 'Latarnik', '978-83-7327-178-4');");

            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (1, 2, 5);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (1, 2, 4);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf, availability) VALUES (1, 2, 4, 0);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (2, 13, 7);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (3, 4, 1);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (7, 2, 7);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (8, 1, 1);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (9, 1, 1);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf, availability) VALUES (4, 4, 1, 0);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf) VALUES (5, 4, 1);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf, availability) VALUES (6, 2, 4, 0);");
            statement.execute("INSERT INTO books (id_title, bookstand, shelf, availability) VALUES (10, 3, 6, 0);");

            statement.execute("INSERT INTO borrowings (id_book, id_reader, date_return) VALUES(3, 5, '2023-11-02');");
            statement.execute("INSERT INTO borrowings (id_book, id_reader, date_return) VALUES(9, 2, '2023-11-20');");
            statement.execute("INSERT INTO borrowings (id_book, id_reader, date_return) VALUES(11, 5, '2024-04-01');");
            statement.execute("INSERT INTO borrowings (id_book, id_reader, date_return) VALUES(12, 1, '2024-03-02');");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
