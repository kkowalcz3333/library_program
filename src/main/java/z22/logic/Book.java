package z22.logic;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Book implements Describable {
    private final int idBook;
    private final int idTitle;
    private final int bookcase;
    private final int shelf;
    private final boolean availability;

    public Book(ResultSet row) {
        try {
            idBook = row.getInt("id_book");
            idTitle = row.getInt("id_title");
            bookcase = row.getInt("bookstand");
            shelf = row.getInt("shelf");
            availability = 0 != row.getInt("availability");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new RuntimeException("Database doesn't match code");
        }
    }

    public int getIdBook() {
        return idBook;
    }

    public int getIdTitle() {
        return idTitle;
    }

    public int getBookcase() {
        return bookcase;
    }

    public int getShelf() {
        return shelf;
    }

    public boolean getAvailability() {
        return availability;
    }

    public String describe() {
        return "Egzemplarz - regał: " + bookcase + ", półka: " + shelf + ", dostępna: " + (availability ? "tak" : "nie") +
                ", ID: " + idBook + ", ID książki: " + idTitle;
    }
}
