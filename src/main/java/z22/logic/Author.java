package z22.logic;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Author implements Describable {
    private final int idAuthor;
    private final String name;
    private final InputStream image; // nullable

    public Author(ResultSet row) {
        this(row, true);
    }

    public Author(ResultSet row, boolean get_image) {
        try {
            idAuthor = row.getInt("id_author");
            name = row.getString("author_name");
            image = get_image ? row.getBinaryStream("author_image") : null;
        } catch (SQLException ex) {
            throw new RuntimeException("Database doesn't match code");
        }
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public String getName() {
        return name;
    }

    public InputStream getImage() {
        return image;
    }

    public String describe() {
        return "Autor - imię: " + name + ", ID: " + idAuthor;
    }
}
