package z22.logic;

/** Common interface for all objects retrieved from database. */
public interface Describable {
    String describe();
}
