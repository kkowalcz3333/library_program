package z22.logic;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;

public class Borrowing implements Describable {
    private final int idBorrowing;
    private final int idBook;
    private final int idReader;
    private final Date dateReturn;

    public Borrowing(ResultSet row) {
        try {
            idBorrowing = row.getInt("id_borrowing");
            idBook = row.getInt("id_book");
            idReader = row.getInt("id_reader");
            dateReturn = row.getDate("date_return");
        } catch (SQLException ex) {
            throw new RuntimeException("Database doesn't match code");
        }
    }

    public int getIdBorrowing() {
        return idBorrowing;
    }

    public int getIdBook() {
        return idBook;
    }

    public int getIdReader() {
        return idReader;
    }

    public Date getDateReturn() {
        return dateReturn;
    }

    public int getDelay() {
        return Math.max(0, (int) ((System.currentTimeMillis() - dateReturn.getTime()) / (24 * 60 * 60 * 1000)));
    }

    public String describe() {
        return "Wypożyczenie - data zwrotu: " + dateReturn.toString() + ", ID wypożyczenia: " + idBorrowing +
                ", ID egzemplarza: " + idBook + ", ID czytelnika: " + idReader;
    }
}
