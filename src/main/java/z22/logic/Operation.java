package z22.logic;

/** Interface of an object for the operations list. It can send its parameters/description and carry itself out. */
public interface Operation {
    String[] getDetails();
    String describe();
    void execute();

}
