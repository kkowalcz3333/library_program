package z22.logic;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
* Stores data from config file, can load it and save it with notifying DatabaseManager.
* databaseNamePrefix: the program has this "namespace" for database names with all privileges granted to avoid conflicts with other databases;
* databaseUser, databasePassword: passed to SQL server;
* daysToReturnBook: when borrowing a book the return date is set n days later;
* paymentPerDay: how many gr will a reader have to pay for each day of being late with returning the book;
*/
public class Config {
    private final String databaseNamePrefix;
    private final String databaseUser;
    private final String databasePassword;
    private final int daysToReturnBook;
    private final int paymentPerDay;

    public Config(String argDatabaseNamePrefix, String argDatabaseUser, String argDatabasePassword,
                  int argDaysToReturnBook, int argPaymentPerDay) {
        databaseNamePrefix = argDatabaseNamePrefix;
        databaseUser = argDatabaseUser;
        databasePassword = argDatabasePassword;
        daysToReturnBook = argDaysToReturnBook;
        paymentPerDay = argPaymentPerDay;
    }

    public Config(String filePath) throws IOException {
        Properties configFile = new Properties();
        configFile.load(new FileInputStream(filePath));

        databaseNamePrefix = configFile.getProperty("databaseNamePrefix");
        databaseUser = configFile.getProperty("databaseUser");
        databasePassword = configFile.getProperty("databasePassword");
        daysToReturnBook = Integer.parseInt(configFile.getProperty("daysToReturnBook"));
        paymentPerDay = Integer.parseInt(configFile.getProperty("paymentPerDay"));
    }

    public void set(String filePath) throws IOException {
        Properties configFile = new Properties();
        configFile.load(new FileInputStream(filePath));

        configFile.setProperty("databaseNamePrefix", databaseNamePrefix);
        configFile.setProperty("databaseUser", databaseUser);
        configFile.setProperty("databasePassword", databasePassword);
        configFile.setProperty("daysToReturnBook", Integer.toString(daysToReturnBook));
        configFile.setProperty("paymentPerDay", Integer.toString(paymentPerDay));

        configFile.store(new FileOutputStream(filePath), null);
    }

    public String getDatabaseNamePrefix() {
        return databaseNamePrefix;
    }

    public String getDatabaseUser() {
        return databaseUser;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public int getDaysToReturnBook() {
        return daysToReturnBook;
    }

    public int getPaymentPerDay() {
        return paymentPerDay;
    }

}
