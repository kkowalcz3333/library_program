package z22.logic;

import java.util.ArrayList;

/**
* The "static" class for storing, accessing, adding/cancelling and executing operations from the "operations" tab.
* The operations are added using the provided methods, they can be removed or executed by their index or all at once.
* Operations from "edition mode" are not included here (except for removing entries, for extra safety).
* */
public final class OperationManager {
    private static final ArrayList<Operation> operations = new ArrayList<>();

    public static ArrayList<Operation> getOperations() {
        return operations;
    }

    public static void removeOperation(int index) {
        operations.remove(index);
    }

    public static void executeOperation(int index) {
        operations.get(index).execute();
        removeOperation(index);
    }

    public static void removeAllOperations() {
        operations.clear();
    }

    public static void executeAllOperations() {
        for (Operation operation : operations) {
            operation.execute();
        }
        removeAllOperations();
    }

    public static void addOperationTakePayment(Reader reader, int valueGr) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {reader},
                        "Wpłać (" + Reader.getFormattedPayment(valueGr) + ") na konto czytelnika (" + reader.getName() + ")");
            }
            public void execute() {
                DatabaseManager.takePayment(reader, valueGr);
            }
        });
    }

    public static void addOperationBorrowBook(Title title, Book book, Reader reader) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {title, book, reader},
                        "Wypożycz książkę (" + title.getTitle() + ") czytelnikowi (" + reader.getName() + ")");
            }
            public void execute() {
                DatabaseManager.borrowBook(title, book, reader);
            }
        });
    }

    public static void addOperationReturnBook(Borrowing borrowing, Title title, Book book, Reader reader) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {borrowing, title, book, reader},
                        "Przyjmij książkę (" + title.getTitle() + ") od czytelnika (" + reader.getName() +
                                ") i nalicz należność (" + DatabaseManager.calculateFormattedPayment(borrowing) + ")"
                );
            }
            public void execute() {
                DatabaseManager.returnBook(borrowing, title, book, reader);
            }
        });
    }

    public static void addOperationRemoveBook(Title title, Book book) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {title, book},
                        "Usuń egzemplarz książki (" + title.getTitle() + ") z regału (" + book.getBookcase() + ") i półki (" + book.getShelf() + ")");
            }
            public void execute() {
                DatabaseManager.removeBook(book);
            }
        });
    }

    public static void addOperationRemoveTitle(Title title) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {title},
                        "Usuń książkę (" + title.getTitle() + ") i jej wszystkie egzemplarze");
            }
            public void execute() {
                DatabaseManager.removeTitle(title);
            }
        });
    }

    public static void addOperationRemoveReader(Reader reader) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {reader},
                        "Usuń czytelnika (" + reader.getName() + ")");
            }
            public void execute() {
                DatabaseManager.removeReader(reader);
            }
        });
    }

    public static void addOperationRemoveAuthor(Author author) {
        operations.add( new AbstractOperation() {
            {
                super.initialize( new Describable[] {author},
                        "Usuń autora (" + author.getName() + "), jego wszystkie tytuły i wszystkie egzemplarze tych tytułów");
            }
            public void execute() {
                DatabaseManager.removeAuthor(author);
            }
        });
    }
}
