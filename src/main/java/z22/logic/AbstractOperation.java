package z22.logic;

/** Common fields and implementation of common methods for objects implementing Operation. */
public abstract class AbstractOperation implements Operation {
    protected String description;
    protected String[] details;

    public void initialize(Describable[] argParameters, String argDescription) {
        description = argDescription;
        details = new String[argParameters.length];
        for (int i = 0; i < argParameters.length; i++) {
            details[i] = argParameters[i].describe();
        }
    }
    public String describe() {
        return description;
    }

    public String[] getDetails() {
        return details;
    }

    public abstract void execute();
}
