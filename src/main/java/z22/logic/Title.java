package z22.logic;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Title implements Describable {
    private final int idTitle;
    private final int idAuthor;
    private final String title;
    private final String isbn; // nullable
    private final String publisher; // nullable
    private final Integer yearPublished; // nullable
    private final Integer pages; // nullable
    private final InputStream image; // nullable

    public Title(ResultSet row) {
        this(row, true);
    }

    public Title(ResultSet row, boolean get_image) {
        try {
            idTitle = row.getInt("id_title");
            idAuthor = row.getInt("id_author");
            title = row.getString("title");
            isbn = row.getString("isbn");
            publisher = row.getString("publisher");
            yearPublished = (Integer) row.getObject("year_published");
            pages = (Integer) row.getObject("pages");
            image = get_image ? row.getBinaryStream("title_image") : null;
        } catch (SQLException ex) {
            throw new RuntimeException("Database doesn't match code");
        }
    }

    public int getIdTitle() {
        return idTitle;
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public Integer getYearPublished() {
        return yearPublished;
    }

    public Integer getPages() {
        return pages;
    }

    public InputStream getImage() {
        return image;
    }

    public String describe() {
        return "Książka - tytuł: " + title +
                (isbn == null ? "" : ", ISBN: " + isbn) +
                (publisher == null ? "" : ", wydawnictwo: " + publisher) +
                (yearPublished == null ? "" : ", rok wydania: " + yearPublished) +
                (pages == null ? "" : ", liczba stron: " + pages) +
                ", ID książki: " + idTitle +
                ", ID autora: " + idAuthor;
    }
}
