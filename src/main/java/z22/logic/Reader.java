package z22.logic;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Reader implements Describable {
    private final int idReader;
    private final String name;
    private final int payment;
    private final Long phoneNumber; // nullable
    private final String email; // nullable

    public Reader(ResultSet row) {
        try {
            idReader = row.getInt("id_reader");
            name = row.getString("reader_name");
            payment = row.getInt("payment");
            phoneNumber = (Long) row.getObject("phone_number");
            email = row.getString("email");
        } catch (SQLException ex) {
            throw new RuntimeException("Database doesn't match code");
        }
    }

    public int getIdReader() {
        return idReader;
    }

    public String getName() {
        return name;
    }

    public int getPayment() {
        return payment;
    }

    public String getFormattedPayment() {
        return Reader.getFormattedPayment(payment);
    }

    public static String getFormattedPayment(int value) {
        int zl = value / 100;
        int gr = value % 100;
        return zl + "," + (gr < 10 ? "0" + gr : gr) + " zł";
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String describe() {
        return "Czytelnik - imię: " + name +
                ", kwota do zapłaty: " + getFormattedPayment() +
                (phoneNumber == null ? "" : ", numer telefonu: " + phoneNumber) +
                (email == null ? "" : ", adres e-mail: " + email) +
                ", ID czytelnika: " + idReader;
    }
}
