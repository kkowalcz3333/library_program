package z22.logic;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/** "Static" class collecting common functions operating on files. */
public class FileProcessor {
    /** Read a picture from resources and turn it into a hexstring to put it into the DB: (..., x?); */
    public static String resourceToHex(String filePath) throws IOException {
        InputStream file = Objects.requireNonNull(FileProcessor.class.getResourceAsStream(filePath));
        byte[] bytes = file.readAllBytes();
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b: bytes) sb.append(String.format("%02x", b));
        return sb.toString();
    }

    /** Read a picture from files and turn it into a hexstring to put it into the DB: (..., x?); */
    public static String fileToHex(String filePath) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b: bytes) sb.append(String.format("%02x", b));
        return sb.toString();
    }

    /** Read a text file from resources.*/
    public static String resourceToString(String filePath) {
        try {
            InputStream file = Objects.requireNonNull(FileProcessor.class.getResourceAsStream(filePath));
            return new String(file.readAllBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "";
    }
}
