package z22.gui;

import z22.Main;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/** "Static" class containing GUI elements that might be reused in multiple classes. */
public class CommonElements {

    /** Create a yes-or-no dialog window and return the answer as boolean. */
    public static boolean questionWindow(String question) {
        String[] options = {"Tak", "Nie"};
        int answer = JOptionPane.showOptionDialog( Main.mainFrame, question, "", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]
        );
        return answer == 0;
    }

    /** Wrap the original border in two empty ones, the amount of pixels can be specified. */
    public static CompoundBorder padded(Border border) {
        return padded(border, 10);
    }

    /** Wrap the original border in two empty ones, the amount of pixels can be specified. */
    public static CompoundBorder padded(Border border, int value) {
        EmptyBorder padding = new EmptyBorder(value, value, value, value);
        return new CompoundBorder(new CompoundBorder(padding, border), padding);
    }
}
