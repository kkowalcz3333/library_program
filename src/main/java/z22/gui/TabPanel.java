package z22.gui;

import z22.Main;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/** The upper panel of the window containing tabs. */
public class TabPanel extends JPanel {
    private final ButtonGroup groupOfButtons;
    private final JToggleButton[] buttons;

    public TabPanel() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        groupOfButtons = new ButtonGroup();
        buttons = new JToggleButton[7];
        JPanel[] panels = new JPanel[] {
                makeButtonWithPanel("/tab_icons/base.png", "Baza", 0),
                makeButtonWithPanel("/tab_icons/books.png", "Księgozbiór", 1),
                makeButtonWithPanel("/tab_icons/readers.png", "Czytelnicy", 2),
                makeButtonWithPanel("/tab_icons/authors.png", "Autorzy", 3),
                makeButtonWithPanel("/tab_icons/delays.png", "Opóźnienia", 4),
                makeButtonWithPanel("/tab_icons/operations.png", "Operacje", 5),
                makeButtonWithPanel("/tab_icons/help.png", "Pomoc", 6),
        };

        for (int i = 0; i < panels.length ; i++) {
            int copyI = i;
            buttons[i].addActionListener(e -> Main.updatePanel(copyI));
            add(panels[i]);
        }
    }

    private JPanel makeButtonWithPanel(String filepath, String labelText, int index) {
        buttons[index] = new JToggleButton(new ImageIcon(Objects.requireNonNull(TabPanel.class.getResource(filepath))));
        buttons[index].setAlignmentX(CENTER_ALIGNMENT);
        buttons[index].setBackground(Color.LIGHT_GRAY);
        buttons[index].setMargin(new Insets(0, 0, 0, 0));
        groupOfButtons.add(buttons[index]);

        JLabel label = new JLabel(labelText);
        label.setAlignmentX(CENTER_ALIGNMENT);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(buttons[index]);
        panel.add(label);

        return panel;
    }
}
