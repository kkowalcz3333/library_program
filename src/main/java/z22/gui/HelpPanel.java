package z22.gui;

import z22.logic.FileProcessor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class HelpPanel extends UpdatablePanel {
    private JTextArea textHelp;

    public HelpPanel() {
        setLayout(new BorderLayout());

        // panelText needs to be created first
        JPanel text = panelText();
        add(panelButtons(), BorderLayout.WEST);
        add(text, BorderLayout.CENTER);

    }

    private JPanel panelButtons() {
        JPanel panelButtons = new JPanel();
        panelButtons.setBorder(new EmptyBorder(10, 10, 10, 10));

        ButtonGroup groupOfButtons = new ButtonGroup();
        JToggleButton[] buttons = new JToggleButton[] {
                new JToggleButton("Ogólne"),
                new JToggleButton("Typowe przypadki użycia"),
                new JToggleButton("Baza"),
                new JToggleButton("Księgozbiór"),
                new JToggleButton("Czytelnicy"),
                new JToggleButton("Autorzy"),
                new JToggleButton("Opóźnienia"),
                new JToggleButton("Operacje"),
        };
        panelButtons.setLayout(new GridLayout(buttons.length, 1, 0, 0));

        for (int i = 0; i < buttons.length; i++) {
            int copyI = i;
            groupOfButtons.add(buttons[i]);
            buttons[i].addActionListener(e -> textHelp.setText(helpContent(copyI)));
            panelButtons.add(buttons[i]);
        }
        groupOfButtons.setSelected(groupOfButtons.getElements().nextElement().getModel(), true);
        return panelButtons;
    }

    private JPanel panelText() {
        JPanel panelText = new JPanel();
        panelText.setLayout(new GridLayout(1, 1, 10, 10));
        panelText.setBorder(CommonElements.padded(BorderFactory.createLineBorder(Color.BLACK)));

        textHelp = new JTextArea(helpContent(0));
        textHelp.setWrapStyleWord(true);
        textHelp.setLineWrap(true);

        JScrollPane scrollPanel = new JScrollPane(textHelp);
        scrollPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        panelText.add(scrollPanel);
        return panelText;
    }

    public void updateConnection() {}

    public void updateEdition() {}

    private String helpContent(int index) {
        return switch (index) {
            case 0 -> FileProcessor.resourceToString("/help/general.txt");
            case 1 -> FileProcessor.resourceToString("/help/use_cases.txt");
            case 2 -> FileProcessor.resourceToString("/help/base.txt");
            case 3 -> FileProcessor.resourceToString("/help/titles.txt");
            case 4 -> FileProcessor.resourceToString("/help/readers.txt");
            case 5 -> FileProcessor.resourceToString("/help/authors.txt");
            case 6 -> FileProcessor.resourceToString("/help/delays.txt");
            case 7 -> FileProcessor.resourceToString("/help/operations.txt");
            default -> "error";
        };
    }
}
