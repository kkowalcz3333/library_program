package z22.gui;

import javax.swing.*;

/** "Interface" of panels that will be used as tabs. */
public abstract class UpdatablePanel extends JPanel {
    /** What the panel should disable if the database is disconnected. It might call updateEdition() as well.*/
    public abstract void updateConnection();

    /** What the panel should disable if entry edition is not allowed. */
    public abstract void updateEdition();
}
