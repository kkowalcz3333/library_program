package z22.gui;

import z22.Main;
import z22.logic.*;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

public class OperationsPanel extends UpdatablePanel {
    JTextArea textDetails;
    JList<String> listOperations;
    JButton buttonConfirm;
    JButton buttonConfirmAll;
    JButton buttonCancel;
    JButton sampleAdd; // TODO: remove when things adding operations are implemented

    public OperationsPanel() {
        setLayout(new BorderLayout());

        add(valuesPanel(), BorderLayout.CENTER);
        add(optionsPanel(), BorderLayout.EAST);
        add(detailsPanel(), BorderLayout.SOUTH);
    }

    public void updateConnection() {
        if (!Main.databaseConnected) {
            OperationManager.removeAllOperations();
            if (textDetails != null) textDetails.setText("");
        }
        if (listOperations != null)   listOperations.setEnabled(Main.databaseConnected);
        if (buttonConfirm != null)    buttonConfirm.setEnabled(Main.databaseConnected);
        if (buttonConfirmAll != null) buttonConfirmAll.setEnabled(Main.databaseConnected);
        if (buttonCancel != null)     buttonCancel.setEnabled(Main.databaseConnected);
        if (sampleAdd != null)        sampleAdd.setEnabled(Main.databaseConnected);

        updateState();
    }

    public void updateEdition() {}

    private void updateState() {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (Operation operation : OperationManager.getOperations()) {
            model.addElement(operation.describe());
        }
        listOperations.setModel(model);
    }

    private JPanel detailsPanel() {
        JPanel detailsPanel = new JPanel();
        detailsPanel.setLayout(new GridLayout(1, 1, 10, 10));
        detailsPanel.setBorder(CommonElements.padded(BorderFactory.createEtchedBorder(EtchedBorder.RAISED)));

        textDetails = new JTextArea();
        textDetails.setWrapStyleWord(true);
        textDetails.setLineWrap(true);
        detailsPanel.add(textDetails);

        return detailsPanel;
    }

    private JPanel optionsPanel() {
        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new GridLayout(4, 1, 10, 10));
        optionsPanel.setBorder(CommonElements.padded(BorderFactory.createRaisedBevelBorder()));

        buttonConfirm = new JButton("Zatwierdź operację");
        buttonConfirmAll = new JButton("Zatwierdź wszystkie");
        buttonCancel = new JButton("Anuluj operację");
        sampleAdd = new JButton("Przykładowe operacje");

        buttonConfirm.addActionListener(e -> {
            if (listOperations.isSelectionEmpty()) return;
            if (!CommonElements.questionWindow("Czy na pewno potwierdzić operację?")) return;

            OperationManager.executeOperation(listOperations.getSelectedIndex());
            updateState();
        });
        buttonConfirmAll.addActionListener(e -> {
            if (!CommonElements.questionWindow("Czy na pewno potwierdzić wszystkie operacje?")) return;

            OperationManager.executeAllOperations();
            updateState();
        });
        buttonCancel.addActionListener(e -> {
            if (listOperations.isSelectionEmpty()) return;
            if (!CommonElements.questionWindow("Czy na pewno anulować operację?")) return;

            OperationManager.removeOperation(listOperations.getSelectedIndex());
            updateState();
        });
        sampleAdd.addActionListener( e -> {
            Borrowing[] borrowings = DatabaseManager.resultQuery("SELECT * FROM borrowings;", Borrowing::new, Borrowing[]::new);
            Author[] authors = DatabaseManager.resultQuery("SELECT * FROM authors;", Author::new, Author[]::new);
            Reader[] readers = DatabaseManager.resultQuery("SELECT * FROM readers;", Reader::new, Reader[]::new);
            Title[] titles = DatabaseManager.resultQuery("SELECT * FROM titles;", Title::new, Title[]::new);
            Book[] books = DatabaseManager.resultQuery("SELECT * FROM books;", Book::new, Book[]::new);

            OperationManager.addOperationBorrowBook(titles[4], books[9], readers[0]); // book (10) of w pustyni (5) to ireneusz
            OperationManager.addOperationReturnBook(borrowings[1], titles[3], books[8], readers[1]); // book (9) of igrzyska smierci (4) from maria
            OperationManager.addOperationTakePayment(readers[4], 1500); // krystyna
            OperationManager.addOperationRemoveAuthor(authors[0]); // unknown author
            OperationManager.addOperationRemoveBook(titles[1], books[3]); // book (4) of quo vadis (2)
            OperationManager.addOperationRemoveTitle(titles[9]); // latarnik
            OperationManager.addOperationRemoveReader(readers[2]); // beata

            updateState();
        });

        optionsPanel.add(buttonConfirm);
        optionsPanel.add(buttonConfirmAll);
        optionsPanel.add(buttonCancel);
        optionsPanel.add(sampleAdd);

        return optionsPanel;
    }

    private JPanel valuesPanel() {
        listOperations = new JList<>();
        JPanel valuesPanel = new JPanel();
        valuesPanel.setLayout(new GridLayout(1, 1, 10, 10));
        valuesPanel.setBorder(CommonElements.padded(BorderFactory.createLineBorder(Color.BLACK)));

        JScrollPane scrollPanel = new JScrollPane(listOperations);

        updateState();
        listOperations.addListSelectionListener(e -> {
            int i = listOperations.getSelectedIndex();
            if (i == -1) return;

            Operation operation = OperationManager.getOperations().get(i);
            StringBuilder result = new StringBuilder();
            for (String description : operation.getDetails()) {
                result.append(description).append("\n");
            }
            result.deleteCharAt(result.length() - 1);
            textDetails.setText(result.toString());
        });

        valuesPanel.add(scrollPanel);

        return valuesPanel;
    }
}
