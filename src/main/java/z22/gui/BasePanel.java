package z22.gui;

import z22.Main;
import z22.logic.Config;
import z22.logic.DatabaseManager;
import z22.logic.OperationManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

public class BasePanel extends UpdatablePanel {
    private boolean internalEditionEnabled;

    private JButton buttonConnection;
    private JButton buttonEdition;
    private JComboBox<String> comboName;
    private JLabel labelConnection;
    private JLabel labelEdition;

    private JButton buttonDeleteBase;
    private JButton buttonNewBase;

    private JTextField configUser;
    private JPasswordField configPassword;
    private JTextField configPrefix;
    private JTextField configDays;
    private JTextField configPayment;
    private JButton buttonCancelSettings;
    private JButton buttonConfirmSettings;

    public BasePanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel settings2 = settings2Panel();
        JPanel settings3 = settings3Panel();

        // settings1 goes last because it needs all other elements created
        add(settings1Panel());
        add(settings2);
        add(settings3);
    }

    private JPanel settings1Panel() {
        JPanel settings1 = new JPanel();
        settings1.setBorder(new EmptyBorder(10, 10, 10, 10));
        settings1.setLayout(new GridLayout(3, 3, 10, 10));

        labelConnection = new JLabel("");
        buttonConnection = new JButton("");
        comboName = new JComboBox<>(DatabaseManager.getDatabaseNames());
        labelEdition = new JLabel("");
        buttonEdition = new JButton("");

        settings1.add(new JLabel("Status:"));
        settings1.add(labelConnection);
        settings1.add(buttonConnection);
        settings1.add(new JLabel("Nazwa:"));
        settings1.add(new JLabel(""));
        settings1.add(comboName);
        settings1.add(new JLabel("Tryb edycji:"));
        settings1.add(labelEdition);
        settings1.add(buttonEdition);

        buttonConnection.addActionListener(e -> {
            if (!OperationManager.getOperations().isEmpty() &&
                !CommonElements.questionWindow("Niezrealizowane operacje zostaną utracone. Czy chcesz rozłączyć się z bazą?")
            ) return;

            if (comboName.getItemCount() == 0) {
                JOptionPane.showMessageDialog(Main.mainFrame, "Nie ma wybranej bazy danych.");
                return;
            }
            Main.databaseConnected = !Main.databaseConnected;
            Main.editionAllowed = internalEditionEnabled && Main.databaseConnected;
            Main.updateConnection();

            if (Main.databaseConnected) {
                try {
                    DatabaseManager.reconnect();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(Main.mainFrame, "Brak dostępu do bazy danych.");
                    return;
                }
                DatabaseManager.setBase((String) comboName.getSelectedItem());
            }
        });

        buttonEdition.addActionListener(e -> {
            internalEditionEnabled = !internalEditionEnabled;
            Main.editionAllowed = internalEditionEnabled && Main.databaseConnected;
            Main.updateEdition();
        });

        updateConnection();
        return settings1;
    }

    /*
    * Here: !internalEditionEnabled or Main.databaseConnected disables edition
    * In other tabs: !Main.editionAllowed disables edition;
    *                !Main.databaseConnected disables edition and interaction with DB
    * */

    public void updateEdition() {
        updateConnection();
    }

    public void updateConnection() {
        if (Main.databaseConnected) {
            labelConnection.setText("podłączona");
            buttonConnection.setText("Odłącz");
        }
        else {
            labelConnection.setText("odłączona");
            buttonConnection.setText("Podłącz");
        }

        if (Main.editionAllowed) {
            labelEdition.setText("włączony");
            buttonEdition.setText("Wyłącz");
        }
        else if (internalEditionEnabled) {
            labelEdition.setText("tylko tutaj");
            buttonEdition.setText("Wyłącz");
        }
        else {
            labelEdition.setText("wyłączony");
            buttonEdition.setText("Włącz");
        }

        boolean state = internalEditionEnabled && !Main.databaseConnected;
        comboName.setEnabled(!Main.databaseConnected);
        buttonDeleteBase.setEnabled(state);
        buttonNewBase.setEnabled(state);
        configUser.setEnabled(state);
        configPassword.setEnabled(state);
        configPrefix.setEnabled(state);
        configDays.setEnabled(state);
        configPayment.setEnabled(state);
        buttonCancelSettings.setEnabled(state);
        buttonConfirmSettings.setEnabled(state);
    }

    private JPanel settings2Panel() {
        JPanel settings2 = new JPanel();
        settings2.setBorder(new EmptyBorder(10, 10, 10, 10));
        settings2.setLayout(new GridLayout(1, 2, 10, 10));

        buttonDeleteBase = new JButton("Usuń bazę...");
        buttonNewBase = new JButton("Nowa baza...");

        buttonDeleteBase.addActionListener(e -> {if(comboName.getItemCount() != 0) deleteBase();});
        buttonNewBase.addActionListener(e -> createBase());

        settings2.add(buttonDeleteBase);
        settings2.add(buttonNewBase);

        return(settings2);
    }

    private void deleteBase() {
        Object[] possibilities = DatabaseManager.getDatabaseNames();
        String name = (String) JOptionPane.showInputDialog(
                Main.mainFrame, "Wybierz bazę do usunięcia:", "Usuń bazę danych",
                JOptionPane.PLAIN_MESSAGE, null, possibilities, possibilities[0]
        );
        if (name != null) {
            DatabaseManager.deleteBase(name);
            comboName.removeItem(name);
        }
    }

    private void createBase() {
        String name = JOptionPane.showInputDialog(Main.mainFrame, "Nazwij bazę danych:", "", JOptionPane.PLAIN_MESSAGE);
        if (name == null) return;
        if (name.isEmpty() || Arrays.asList(DatabaseManager.getDatabaseNames()).contains(name)) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Nazwa jest pusta lub się powtarza.");
            return;
        }

        boolean addSampleData = CommonElements.questionWindow("Czy wstawić przykładowe dane do bazy?");

        name = name.replaceAll("[\0\\x08\\x09\\x1a\\n\\r\"'\\\\%]", "");
        DatabaseManager.createBase(name, addSampleData);
        comboName.addItem(name);
    }

    private JPanel settings3Panel() {
        JPanel settings3 = new JPanel();
        settings3.setBorder(new EmptyBorder(10, 10, 10, 10));
        settings3.setLayout(new GridLayout(3, 4, 10, 10));

        buttonCancelSettings = new JButton("Cofnij");
        buttonConfirmSettings = new JButton("Zatwierdź");
        configUser = new JTextField();
        configPassword = new JPasswordField();
        configPrefix = new JTextField();
        configDays = new JTextField();
        configPayment = new JTextField();
        resetConfig();

        buttonCancelSettings.addActionListener(e -> resetConfig());
        buttonConfirmSettings.addActionListener(e -> setConfig());

        settings3.add(new JLabel("Prefix nazw BD:"));
        settings3.add(configPrefix);
        settings3.add(new JLabel("Nazwa użytkownika SQL:"));
        settings3.add(configUser);
        settings3.add(new JLabel("Dni na zwrot książki:"));
        settings3.add(configDays);
        settings3.add(new JLabel("Hasło użytkownika SQL:"));
        settings3.add(configPassword);
        settings3.add(new JLabel("Koszt przetrzymania (gr/d):"));
        settings3.add(configPayment);
        settings3.add(buttonCancelSettings);
        settings3.add(buttonConfirmSettings);

        return settings3;
    }

    private void resetConfig() {
        try {
            Config config = new Config(DatabaseManager.getConfigPath());
            configPrefix.setText(config.getDatabaseNamePrefix());
            configUser.setText(config.getDatabaseUser());
            configPassword.setText(config.getDatabasePassword());
            configDays.setText(String.valueOf(config.getDaysToReturnBook()));
            configPayment.setText(String.valueOf(config.getPaymentPerDay()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setConfig() {
        if (configPrefix.getText().isEmpty() || configUser.getText().isEmpty() || configDays.getText().isEmpty() ||
                new String(configPassword.getPassword()).isEmpty() || configDays.getText().isEmpty()) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Niektóre dane są puste.");
            return;
        }
        try {
            Config config = new Config(
                configPrefix.getText(),
                configUser.getText(),
                new String(configPassword.getPassword()),
                Integer.parseInt(configDays.getText()),
                Integer.parseInt(configPayment.getText())
            );
            config.set(DatabaseManager.getConfigPath());
            DatabaseManager.loadConfig();
            DatabaseManager.reconnect();
            comboName.removeAllItems();
            for (String name : DatabaseManager.getDatabaseNames()) {
                comboName.addItem(name);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(Main.mainFrame, "Łączenie z bazą przy tej konfiguracji nieudane.");
            comboName.removeAllItems();
        }
    }
}
