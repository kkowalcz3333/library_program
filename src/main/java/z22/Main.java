package z22;

import z22.logic.DatabaseManager;
import javax.swing.*;
import z22.gui.*;
import java.sql.SQLException;

public class Main {
    public static JFrame mainFrame;
    private static JPanel mainPanel;
    private static UpdatablePanel[] panels;
    private static UpdatablePanel currentPanel;

    public static boolean databaseConnected = false;
    public static boolean editionAllowed = false;

    public static void main(String[] args) {
        try {
            DatabaseManager.connect();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        SwingUtilities.invokeLater( () -> {
            mainFrame = new JFrame();
            mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            mainPanel = new JPanel();
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

            panels = new UpdatablePanel[7];
            panels[0] = new BasePanel();
            panels[1] = new BooksPanel();
            panels[2] = new ReadersPanel();
            panels[3] = new AuthorsPanel();
            panels[4] = new DelaysPanel();
            panels[5] = new OperationsPanel();
            panels[6] = new HelpPanel();
            updateConnection();
            currentPanel = panels[0];

            TabPanel tabPanel = new TabPanel();

            mainPanel.add(tabPanel);
            mainPanel.add(panels[0]);

            mainFrame.setContentPane(mainPanel);
            mainFrame.pack();
            mainFrame.setVisible(true);
        });


    }

    public static void updatePanel(int index) {
        SwingUtilities.invokeLater( () -> {
            mainPanel.remove(currentPanel);
            mainPanel.add(panels[index]);
            currentPanel = panels[index];

            // these need to check for new entries whenever they're called
            if (index == 4 || index == 5) currentPanel.updateConnection();

            refresh();
        });
    }

    public static void updateConnection() {
        for (UpdatablePanel panel : panels) {
            panel.updateConnection();
        }
        refresh();
    }

    public static void updateEdition() {
        for (UpdatablePanel panel : panels) {
            panel.updateEdition();
        }
        refresh();
    }

    public static void refresh() {
        mainFrame.revalidate();
        mainFrame.repaint();
    }
}
